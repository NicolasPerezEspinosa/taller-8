import express from 'express';

let app= new express();

//ruta saludo y su funcion
function saludar(peticion, respuesta){
    respuesta.send("Hola API!!");
};
app.get('/saludo', saludar);

//ruta calculadora y su funcion
function calcular(req, res){
    let miSumador= new Sumador();
    let resultado=miSumador.sumar(56,78); 

    res.send(resultado.toString());
};
app.get('/calculadora', calcular);

//ruta aleatorio
function generaraleatorio(req, res){
    let aleatorio;
    aleatorio= Math.round(Math.random()*100);

    res.send("Hola su numero entre el 1 y 100 es: " + aleatorio.toString());
};
app.get('/aleatorio', generaraleatorio);

//abre servidor en el puerto 3000
app.listen(3000);

//clase para usar en la funcion calcular
class Sumador{

    sumar(a,b){
        return a+b;
    }
}